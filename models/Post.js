const Model = require("./Model");
class Post extends Model {
  constructor() {
    super();
    this.database = require("../db/posts.json");
  }

  getAllPosts() {
    return super.getAllData();
  }

  getPostById(id) {
    return super.getById(id);
  }
}

module.exports = Post;
