const Model = require("./Model");
class User extends Model {
  constructor() {
    super();
    this.database = require("../db/users.json");
  }

  getAllUser() {
    return super.getAllData();
  }

  getUserById(id) {
    return super.getById(id);
  }
}

module.exports = User;
