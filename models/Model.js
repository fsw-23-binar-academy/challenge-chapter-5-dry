class Model {
  constructor() {
    this.database = "";
  }

  #getData() {
    return this.database;
  }

  getAllData() {
    return this.#getData();
  }

  getById(id) {
    const data = this.#getData();

    let dataFiltered = data.filter((item) => item.id == id);

    return dataFiltered;
  }
}

module.exports = Model;
