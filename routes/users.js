var express = require("express");
var router = express.Router();
const UserController = require("../controllers/userController");

const userController = new UserController();
/* GET users listing. */
router.get("/", userController.getAllUser);

router.get("/:id", userController.getUserById);

module.exports = router;
