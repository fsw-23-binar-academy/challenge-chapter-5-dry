var express = require("express");
var router = express.Router();
const PostController = require("../controllers/postController");

const postController = new PostController();

/* GET post listing. */
router.get("/", postController.getAllPosts);

/* GET psot by id. */
router.get("/:id", postController.getPostById);

module.exports = router;
